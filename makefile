LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++
CPPFLAGS = -Wall -pedantic -ansi -std=c++14 -I. -I$(INC_DIR)
ARCHIVE = ar

RM = rm -rf
RM_TUDO = rm -fr

PROG = teste

.PHONY: all clean debug doc doxygen valgrind1

all: init linux

linux: init pedroemerick.a pedroemerick.so prog_estatico prog_dinamico

windows: init pedroemerick.lib pedroemerick.dll prog_estatico.exe prog_dinamico.exe

debug: CPPFLAGS += -g -O0
debug: all

valgrind estatico: 
	valgrind --leak-check=full --show-reachable=yes -v ./bin/prog_estatico

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/
	@mkdir -p $(LIB_DIR)/

# LINUX

pedroemerick.a: $(INC_DIR)/buscas.h $(INC_DIR)/quicksort.h $(INC_DIR)/mergesort.h $(INC_DIR)/bubllesort.h $(INC_DIR)/insertionsort.h $(INC_DIR)/selectionsort.h $(INC_DIR)/pilha.h $(INC_DIR)/fila.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/_node.h $(SRC_DIR)/lib.cpp
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/lib.cpp -o $(OBJ_DIR)/lib.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/lib.o
	@echo "+++ [Biblioteca estatica criada em $(LIB_DIR)/$@] +++"

pedroemerick.so: $(INC_DIR)/buscas.h $(INC_DIR)/quicksort.h $(INC_DIR)/mergesort.h $(INC_DIR)/bubllesort.h $(INC_DIR)/insertionsort.h $(INC_DIR)/selectionsort.h $(INC_DIR)/pilha.h $(INC_DIR)/fila.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/_node.h $(SRC_DIR)/lib.cpp
	$(CC) $(CPPFLAGS) -fPIC -c $(SRC_DIR)/lib.cpp -o $(OBJ_DIR)/lib.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/lib.o
	@echo "+++ [Biblioteca dinamica criada em $(LIB_DIR)/$@] +++"

prog_estatico:
	$(CC) $(CPPFLAGS) $(SRC_DIR)/main.cpp $(SRC_DIR)/funcoes_vetor.cpp $(SRC_DIR)/teste_ordenacao.cpp $(SRC_DIR)/teste_busca.cpp $(SRC_DIR)/teste_tads.cpp $(SRC_DIR)/funcoes_excecao.cpp $(LIB_DIR)/pedroemerick.a -o $(BIN_DIR)/$@

prog_dinamico:
	$(CC) $(CPPFLAGS) $(SRC_DIR)/main.cpp $(SRC_DIR)/funcoes_vetor.cpp $(SRC_DIR)/teste_ordenacao.cpp $(SRC_DIR)/teste_busca.cpp $(SRC_DIR)/teste_tads.cpp $(SRC_DIR)/funcoes_excecao.cpp $(LIB_DIR)/pedroemerick.so -o $(BIN_DIR)/$@

# WINDOWS

pedroemerick.lib: $(INC_DIR)/buscas.h $(INC_DIR)/quicksort.h $(INC_DIR)/mergesort.h $(INC_DIR)/bubllesort.h $(INC_DIR)/insertionsort.h $(INC_DIR)/selectionsort.h $(INC_DIR)/pilha.h $(INC_DIR)/fila.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/_node.h $(SRC_DIR)/lib.cpp
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/lib.cpp -o $(OBJ_DIR)/lib.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/lib.o
	@echo "+++ [Biblioteca estatica criada em $(LIB_DIR)/$@] +++"

pedroemerick.dll: $(INC_DIR)/buscas.h $(INC_DIR)/quicksort.h $(INC_DIR)/mergesort.h $(INC_DIR)/bubllesort.h $(INC_DIR)/insertionsort.h $(INC_DIR)/selectionsort.h $(INC_DIR)/pilha.h $(INC_DIR)/fila.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/_node.h $(SRC_DIR)/lib.cpp
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/lib.cpp -o $(OBJ_DIR)/lib.o
	$(CC) -shared -o $(LIB_DIR)/$@ $(OBJ_DIR)/lib.o
	@echo "+++ [Biblioteca dinamica criada em $(LIB_DIR)/$@] +++"

prog_estatico.exe:
	$(CC) $(CPPFLAGS) $(SRC_DIR)/main.cpp $(SRC_DIR)/funcoes_vetor.cpp $(SRC_DIR)/teste_ordenacao.cpp $(SRC_DIR)/teste_busca.cpp $(SRC_DIR)/teste_tads.cpp $(SRC_DIR)/funcoes_excecao.cpp $(LIB_DIR)/pedroemerick.lib -o $(BIN_DIR)/$@

prog_dinamico.exe:
	$(CC) $(CPPFLAGS) $(SRC_DIR)/main.cpp $(SRC_DIR)/funcoes_vetor.cpp $(SRC_DIR)/teste_ordenacao.cpp $(SRC_DIR)/teste_busca.cpp $(SRC_DIR)/teste_tads.cpp $(SRC_DIR)/funcoes_excecao.cpp $(LIB_DIR)/pedroemerick.dll -o $(BIN_DIR)/$@

# DOXYGEN

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

# CLEAN

clean:
	@echo "====================================================="
	@echo "Removendo arquivos objeto e executaveis/binarios ..."
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*

