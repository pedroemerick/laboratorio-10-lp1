/**
* @file     teste_busca.h
* @brief    Declaracao do prototipo da funcao que faz os testes com os algoritmos de busca
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#ifndef TESTE_BUSCA_H
#define TESTE_BUSCA_H

/** 
 * @brief	Função que realiza testes nos algoritmos de busca, implementados na biblioteca
 */
void testes_busca ();

#endif