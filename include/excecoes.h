/**
 * @file	excecoes.h
 * @brief	Implementacao das classes de excecoes
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	23/03/2017
 * @date	25/04/2017
 */

#ifndef EXCECOES_H
#define EXCECOES_H

#include <exception>
using std::exception;

#include <new>
using std::bad_alloc;

/**
 * @brief   Implementações de funções de buscas e ordenações, e 
 *          TADs vistas na disciplina de Estruturas de Banco de Dados I
 *
 * \ingroup EDB1
 */
namespace edb1
{
    /** 
     * @class 	 ElementoNaoEncontrado excecoes.h
     * @brief 	 Classe que representa uma excecao, quando é buscado um elemento, 
     *           atraves de qualquer funcao de busca e nao encontrado
     * @details  O metodo modificado é apenas o 'what'
     */
    class ElementoNaoEncontrado : public exception {
        public:
            /** @brief Retorna a mensagem da excecao encontrada */
            const char* what () {
                return "Elemento não encontrado em sua busca !!!";
            }
    };

    /** 
     * @class 	 VetorVazio excecoes.h
     * @brief 	 Classe que representa uma excecao, quando o vetor tem um tamanho invalido, 
     *           ou vazio
     * @details  O metodo modificado é apenas o 'what'
     */
    class VetorVazio : public exception {
        public:
            /** @brief Retorna a mensagem da excecao encontrada */            
            const char* what () {
                return "Vetor com tamanho invalido !!!";
            }
    };

    /** 
     * @class 	 FilaVazia excecoes.h
     * @brief 	 Classe que representa uma excecao, quando a TAD Fila nao contem elementos
     * @details  O metodo modificado é apenas o 'what'
     */
    class FilaVazia : public exception {
        public:
            /** @brief Retorna a mensagem da excecao encontrada */    
            const char* what () {
                return "Fila vazia/sem elementos !!!";
            }
    };

    /** 
     * @class 	 ListaVazia excecoes.h
     * @brief 	 Classe que representa uma excecao, quando a TAD Lista nao contem elementos
     * @details  O metodo modificado é apenas o 'what'
     */
    class ListaVazia : public exception {
        public:
            /** @brief Retorna a mensagem da excecao encontrada */    
            const char* what () {
                return "Lista vazia/sem elementos !!!";
            }
    };

    /** 
     * @class 	 PilhaVazia excecoes.h
     * @brief 	 Classe que representa uma excecao, quando a TAD Pilha nao contem elementos
     * @details  O metodo modificado é apenas o 'what'
     */
    class PilhaVazia : public exception {
        public:
            /** @brief Retorna a mensagem da excecao encontrada */    
            const char* what () {
                return "Pilha vazia/sem elementos !!!";
            }
    };
} /* namespace edb1 */

#endif