/**
 * @file	insertionsort.h
 * @brief	Implementacao da funcao de ordenacao insertionsort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

#include "excecoes.h"

#include <iostream>
using std::cerr;
using std::endl;

/**
 * @brief   Implementações de funções de buscas e ordenações, e 
 *          TADs vistas na disciplina de Estruturas de Banco de Dados I
 *
 * \ingroup EDB1
 */
namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Insertion Sort
     * @param 	vetor Vetor para ordenacao
     * @param	tamanho Tamanho do vetor
     */
    template < typename T >
    void insertionsort (T *vetor, int tamanho)
    {
        try {
            if (tamanho <= 0)
                throw VetorVazio ();
            else
            {
                for (int ii = 1; ii < tamanho; ii++)
                {
                    int aux = ii;

                    while (aux > 0 && (vetor[aux - 1] > vetor [aux]))
                    {
                        T temp = vetor[aux];
                        vetor[aux] = vetor[aux - 1];
                        vetor[aux - 1] = temp;
                        
                        aux--;
                    }
                }
            }
        } catch (VetorVazio &ex) {
            cerr << ex.what () << endl;
            return;
        } catch (...) {
            cerr << "Erro desconhecido na ordenação !!!" << endl;
            return;
        }
    }
} /* namespace edb1 */

#endif