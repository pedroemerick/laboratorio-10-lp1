/**
 * @file	bubllesort.h
 * @brief	Implementacao da funcao de ordenacao bubllesort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef BUBLLESORT_H
#define BUBLLESORT_H

#include "excecoes.h"

#include <iostream>
using std::cerr;
using std::endl;

/**
 * @brief   Implementações de funções de buscas e ordenações, e 
 *          TADs vistas na disciplina de Estruturas de Banco de Dados I
 *
 * \ingroup EDB1
 */
namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Bublle Sort
     * @param 	vetor Vetor para ordenacao
     * @param	tamanho Tamanho do vetor
     */
    template < typename T >
    void bubllesort (T *vetor, int tamanho)
    {
        try {
            if (tamanho <= 0)
                throw VetorVazio ();
            else 
            {
                for (int ii = 0; ii < (tamanho - 1); ii++)
                {
                    for (int jj = 0; jj < (tamanho - ii - 1); jj++)
                    {
                        if (vetor[jj] > vetor[jj+1])
                        {
                            T temp = vetor[jj + 1];
                            vetor[jj + 1] = vetor[jj];
                            vetor[jj] = temp;
                        }
                    }
                }
            }
        } catch (VetorVazio &ex) {
            cerr << ex.what () << endl;
            return;
        } catch (...) {
            cerr << "Erro desconhecido na ordenação !!!" << endl;
            return;
        }
    }
} /* namespace edb1 */

#endif