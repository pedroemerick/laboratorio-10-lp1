/**
 * @file	selectionsort.h
 * @brief	Implementacao da funcao de ordenacao selectionsort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef SELECTIONSORT_H
#define SELECTIONSORT_H

#include "excecoes.h"

#include <iostream>
using std::cerr;
using std::endl;

/**
 * @brief   Implementações de funções de buscas e ordenações, e 
 *          TADs vistas na disciplina de Estruturas de Banco de Dados I
 *
 * \ingroup EDB1
 */
namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Selection Sort
     * @param 	vetor Vetor para ordenacao
     * @param	tamanho Tamanho do vetor
     */
    template < typename T >
    void selectionsort (T *vetor, int tamanho)
    {
        try {
            if (tamanho <= 0)
                throw VetorVazio ();
            else 
            {
                for (int ii = 0; ii < tamanho-1; ii++)
                {
                    int menor = ii;

                    for (int jj = ii+1; jj < tamanho; jj++)
                    {
                        if (vetor[jj] < vetor[menor])
                        {
                            menor = jj;
                        }
                    }

                    if (menor != ii)
                    {
                        T aux = vetor[ii];
                        vetor[ii] = vetor[menor];
                        vetor[menor] = aux;
                    }
                }
            }
        } catch (VetorVazio &ex) {
            cerr << ex.what () << endl;
            return;
        } catch (...) {
            cerr << "Erro desconhecido na ordenação !!!" << endl;
            return;
        }
    }
} /* namespace edb1 */

#endif