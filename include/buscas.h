/**
 * @file	buscas.h
 * @brief	Implementacao das funcoes de busca
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef BUSCAS_H
#define BUSCAS_H

/**
 * @brief   Implementações de funções de buscas e ordenações, e 
 *          TADs vistas na disciplina de Estruturas de Banco de Dados I
 *
 * \ingroup EDB1
 */
namespace edb1
{
    /** 
     * @brief   Funcao generica que faz uma busca binaria em seu modo iterativo
     * @param   vetor Vetor para busca
     * @param	tamanho Tamanho do vetor
     * @param   elemento Elemento a ser buscado
     * @return  Indice do elemento no vetor se existir senao -1 ou -2
     */
    template < typename T >
    int busca_binaria_iterativa (T *vetor, int tamanho, T elemento)
    {
        if (tamanho <= 0) 
            return -2;

        int inicio = 0;
        int fim = tamanho - 1;
        int meio = (inicio + fim) / 2;

        while (inicio <= fim)
        {
            if (vetor[meio] == elemento)
                return meio;
            else if (vetor[meio] < elemento) 
                inicio = meio + 1;
            else
                fim = meio - 1;

            meio = (inicio + fim) / 2;
        }

        return -1;
    }

    /** 
     * @brief   Funcao generica que faz uma busca binaria em seu modo recursivo
     * @param 	vetor Vetor para busca
     * @param	tamanho Tamanho do vetor
     * @param   elemento Elemento a ser buscado
     * @param   cont Contador para verificar se é vetor com tamanho invalido ou elemento nao encontrado
     * @return  Indice do elemento no vetor se existir senao -1 ou -2
     */
    template < typename T >
    int busca_binaria_recursiva (T *vetor, int tamanho, T elemento, int cont = 0) 
    {
        if (tamanho <= 0)
        {
            if (cont == 0)
                return -2;
            else
                return -1;
        }

        int meio = tamanho / 2;

        if (vetor[meio] == elemento) 
            return meio;
        else if (vetor[meio] > elemento) 
            return busca_binaria_recursiva (vetor, meio, elemento, cont+1); 
        else if (vetor[meio] < elemento) 
            return busca_binaria_recursiva (&vetor[meio+1], (tamanho - meio - 1), elemento, cont+1); 

        return -1;
    }

    /** 
     * @brief   Funcao generica que faz uma busca sequencial a partir do primeiro elemento 
     *          em seu modo recursivo
     * @param 	vetor Vetor para busca
     * @param	tamanho Tamanho do vetor
     * @param   elemento Elemento a ser buscado
     * @param   indice Indice do elemento no vetor
     * @param   cont Contador para verificar se é vetor com tamanho invalido ou elemento nao encontrado
     * @return  Indice do elemento no vetor se existir senao -1 ou -2
     */
    template < typename T >
    int busca_sequencial_recursiva (T *vetor, int tamanho, T elemento, int indice = 0, int cont = 0) 
    {
        if (tamanho <= 0)
        {
            if (cont == 0)
                return -2;
            else
                return -1;
        }

        if (vetor[0] == elemento) 
            return indice;
        else 
            return busca_sequencial_recursiva ((vetor + 1), (tamanho - 1), elemento, indice+1, cont+1);
    }

    /** 
     * @brief   Funcao generica que faz uma busca sequencial a partir do primeiro elemento 
     *          em seu modo iterativo
     * @param 	vetor Vetor para busca
     * @param	tamanho Tamanho do vetor
     * @param   elemento Elemento a ser buscado
     * @return  Indice do elemento no vetor se existir senao -1 ou -2
     */
    template < typename T >
    int busca_sequencial_iterativa (T *vetor, int tamanho, T elemento) 
    {
        if (tamanho <= 0)
            return -2;

        for (int ii = 0; ii < tamanho; ii++)
        {
            if (vetor[ii] == elemento)
                return ii;
        }

        return -1;
    }

    /** 
     * @brief   Funcao generica que faz uma busca ternaria em seu modo recursivo
     * @param 	vetor Vetor para busca
     * @param	inicio Indice do inicio do vetor
     * @param	fim Indice do fim do vetor
     * @param   elemento Elemento a ser buscado
     * @param   cont Contador para verificar se é vetor com tamanho invalido ou elemento nao encontrado     
     * @return  Indice do elemento no vetor se existir senao -1 ou -2
     */
    template < typename T >
    int busca_ternaria_recursiva (T *vetor, int inicio, int fim, T elemento, int cont = 0)
    {
        if (fim < inicio || fim < 0 || inicio < 0)
        {
            if (cont == 0)
                return -2;
            else
                return -1;
        }
        
        int meio1 = (fim - inicio) / 3 + inicio;
        int meio2 = 2 * (fim - inicio) / 3 + inicio;

        if (vetor[meio1] == elemento)
            return meio1;
        else if (vetor[meio2] == elemento)
            return meio2;
        else if (vetor[meio1] < elemento)
            return busca_ternaria_recursiva (vetor, (meio1 + 1), fim, elemento, cont+1);
        else if (vetor[meio2] > elemento)
            return busca_ternaria_recursiva (vetor, inicio, (meio2 - 1), elemento, cont+1);
        else 
            return busca_ternaria_recursiva (vetor, (meio1 + 1), (meio2 - 1), elemento, cont+1);
    }

    /** 
     * @brief   Funcao generica que faz uma busca ternaria em seu modo iterativo
     * @param 	vetor Vetor para busca
     * @param	inicio Indice do inicio do vetor
     * @param	fim Indice do fim do vetor
     * @param   elemento Elemento a ser buscado
     * @return  Indice do elemento no vetor se existir senao -1
     */
    template < typename T >
    int busca_ternaria_iterativa (T *vetor, int inicio, int fim, T elemento) 
    {
        if (fim < inicio || fim < 0 || inicio < 0)
            return -2;

        int meio1, meio2;
        int esq = inicio;
        int dir = fim;
        do 
        {
            meio1 = ((dir - esq) / 3) + esq;
            meio2 = (((dir - esq) / 3) * 2) + esq;

            if(elemento == vetor[meio1])	
                return meio1;
            if(elemento == vetor[meio2]) 
                return meio2;
            if(elemento < vetor[meio1])
                dir = meio1 - 1;
            if(elemento > vetor[meio1] && elemento < vetor[meio2]) {
                esq = meio1 + 1;
                dir = meio2 - 1;
            } else if(elemento > vetor[meio2])
                esq = meio2 + 1;

        } while(esq <= dir);
            
        return -1;
    }
} /* namespace edb1 */

#endif