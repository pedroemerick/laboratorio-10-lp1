/**
 * @file	mergesort.h
 * @brief	Implementacao da funcao de ordenacao mergesort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef MERGESORT_H
#define MERGESORT_H

#include <cstddef>

#include <new>
using std::bad_alloc;

#include "excecoes.h"
using namespace edb1;

#include <iostream>
using std::cerr;
using std::endl;

/**
 * @brief   Implementações de funções de buscas e ordenações, e 
 *          TADs vistas na disciplina de Estruturas de Banco de Dados I
 *
 * \ingroup EDB1
 */
namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a junção de dois vetores criando um outro vetor ordenado
     * @param 	vetor Vetor para ordenacao
     * @param	inicio Indice do inicio do vetor
     * @param	meio Indice do meio do vetor
     * @param	fim Indice do fim do vetor
     */
    template < typename T >
    void merge (T *vetor, int inicio, int meio, int fim)
    {
        int fim1 = 0;
        int fim2 = 0;
        int tamanho = fim - inicio + 1;
        int pivot1 = inicio;
        int pivot2 = meio + 1;

        T *temp;

        try {
            temp = new T [tamanho];
        } catch (bad_alloc &ex) {
            cerr << ex.what () << " Erro na alocacao de memoria !!!" << endl;
            exit (1);
        } catch (...) {
            cerr << "Erro desconhecido na alocacao de memoria !!!" << endl;
            exit (1);
        }

        if (temp != NULL)
        {
            for (int ii = 0; ii < tamanho; ii++)
            {
                if (!fim1 & !fim2)
                {
                    if (vetor[pivot1] < vetor[pivot2])
                        temp[ii] = vetor[pivot1++];
                    else 
                        temp[ii] = vetor[pivot2++];

                    if (pivot1 > meio)
                        fim1 = 1;
                    if (pivot2 > fim)
                        fim2 = 1;
                }
                else 
                {
                    if (!fim1)
                        temp[ii] = vetor[pivot1++];
                    else
                        temp[ii] = vetor[pivot2++];
                }
            }

            for (int jj = 0, kk = inicio; jj < tamanho; jj++, kk++)
            {
                vetor[kk] = temp[jj];
            }
        }

        delete [] temp;
    }

    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Merge Sort
     * @param 	vetor Vetor para ordenacao
     * @param	inicio Indice do inicio do vetor
     * @param	fim Indice do fim do vetor
     */
    template < typename T >
    void mergesort (T *vetor, int inicio, int fim)
    {
        try {
            if (fim < 0 || inicio < 0)
                throw VetorVazio ();
            else
            {
                if (inicio < fim)
                {
                    int meio = (inicio + fim) / 2;
                    
                    mergesort (vetor, inicio, meio);
                    mergesort (vetor, (meio + 1), fim);
                    merge (vetor, inicio, meio, fim);
                }
            }
        } catch (VetorVazio &ex) {
            cerr << ex.what () << endl;
            return;
        } catch (...) {
            cerr << "Erro desconhecido na ordenação !!!" << endl;
            return;
        }
    }
} /* namespace edb1 */

#endif