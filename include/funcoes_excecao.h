/**
* @file     funcoes_excecao.h
* @brief    Declaracao do prototipo da funcao que verifica se ha alguma excecao no retorno de uma busca
* @author   Pedro Emerick (p.emerick@live.com)
* @since    23/06/2017
* @date	    25/06/2017
*/

#ifndef FUNCOES_EXCECAO_H
#define FUNCOES_EXCECAO_H

/** 
 * @brief	Função que verifica se ha alguma excecao no retorno da busca 
 *          e faz o tratamento necessario
 * @param 	indice_resultado Retorno da busca
 */
void ErroBusca (int indice_resultado);

#endif