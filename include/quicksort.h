/**
 * @file	quicksort.h
 * @brief	Implementacao da funcao de ordenacao quicksort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "excecoes.h"

#include <iostream>
using std::cerr;
using std::endl;

/**
 * @brief   Implementações de funções de buscas e ordenações, e 
 *          TADs vistas na disciplina de Estruturas de Banco de Dados I
 *
 * \ingroup EDB1
 */
namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Quick Sort
     * @param 	vetor Vetor para ordenacao
     * @param	inicio Indice do inicio do vetor
     * @param	fim Indice do fim do vetor
     */
    template < typename T >
    void quicksort (T *vetor, int inicio, int fim)
    {
        try {
            if (fim <= 0 || inicio <= 0)
                throw VetorVazio ();
            else
            {
                int aux1 = inicio;
                int aux2 = fim;

                T pivot = vetor [(inicio + fim) / 2];

                while (aux1 <= aux2)
                {
                    while (vetor[aux1] < pivot)
                        aux1++;
                    while (vetor[aux2] > pivot)
                        aux2--;
                    
                    if (aux1 <= aux2)
                    {
                        T temp = vetor[aux1];
                        vetor[aux1] = vetor[aux2];
                        vetor[aux2] = temp;

                        aux1++;
                        aux2--;
                    }
                }
                
                if (inicio < aux2)
                    quicksort (vetor, inicio, aux2);
                if (aux1 < fim)
                    quicksort (vetor, aux1, fim);
            }
        } catch (VetorVazio &ex) {
            cerr << ex.what () << endl;
            return;
        } catch (...) {
            cerr << "Erro desconhecido na ordenação !!!" << endl;
            return;
        }
    }
} /* namespace edb1 */

#endif