/**
* @file     teste_ordenacao.cpp
* @brief    Implementacao da funcao que faz os testes com os algoritmos de ordenacao
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#include "teste_ordenacao.h"

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include "quicksort.h"
#include "mergesort.h"
#include "bubllesort.h"
#include "insertionsort.h"
#include "selectionsort.h"

#include "funcoes_vetor.h"

using namespace edb1;

/** 
 * @brief	Função que realiza testes nos algoritmos de ordenacao, implementados na biblioteca
 */
void testes_ordenacao ()
{
    cout << "O vetor usado nestes testes tem tamanho 10 com valores aleatorios";
    cout << endl << endl;

    int vetor [10];
    vetor_aleatorio_int (vetor, 10);

    cout << "*********************************************" << endl;
    cout << "*       TESTANDO ERROS NAS ORDENACOES       *" << endl;
    cout << "*    PASSANDO O TAMANHO DO VETOR INVALIDO   *" << endl;    
    cout << "*********************************************" << endl << endl;

    /** Testando o quicksort */
    cout << "-> QUICKSORT" << endl << endl;
    cout << "Chamando funcao QuickSort com o fim do vetor com indice -1: " << endl;
    quicksort (vetor, 0, -1);

    /** Testando o insertionsort */
    cout << endl << "-> INSERTIONSORT" << endl << endl;
    cout << "Chamando funcao InsertionSort com o tamanho do vetor 0: " << endl;
    insertionsort (vetor, 0);

    /** Testando o bubllesort */
    cout << endl << "-> BUBLLESORT" << endl << endl;
    cout << "Chamando funcao InsertionSort com o tamanho do vetor -2: " << endl;
    bubllesort (vetor, -2);
  
    /** Testando o mergesort */
    cout << endl << "-> MERGESORT" << endl << endl;
    cout << "Chamando funcao MergeSort com o inicio do vetor com indice -3: " << endl;
    mergesort (vetor, -3, 9);
  
    /** Testando o selectionsort */  
    cout << endl << "-> SELECTIONSORT" << endl << endl;
    cout << "Chamando funcao SelectionSort com o tamanho do vetor 0: " << endl;
    selectionsort (vetor, 0);
}