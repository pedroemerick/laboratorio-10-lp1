/**
* @file     teste_tads.cpp
* @brief    Implementacao das funcoes que fazem os testes com os tipos abstratos de dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#include "teste_tads.h"

#include "pilha.h"
#include "ll_dupla_ord.h"
#include "fila.h"

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <string>
using std::string;

#include <sstream>
using std::stringstream;

using namespace edb1;

/** 
 * @brief	Função que realiza testes no tad lista, implementados na biblioteca
 */
void teste_lista ()
{
    /** Testando a lista ligada duplamente encadeada */
    cout << "-> LISTA" << endl << endl;
    
    Lista <int> lista;

    /** Testando lista de inteiros */
    cout << "Tentando remover elemento da lista vazia: " << endl;
    
    cout << "Removendo elemento 13 da lista ..." << endl;
    lista.Remover (13);  
}

/** 
 * @brief	Função que realiza testes no tad fila, implementados na biblioteca
 */
void teste_fila ()
{
    /** Testando a fila */
    Fila <int> fila;

    int opcao = -1;
    while (opcao != 0)
    {
        cout << endl;
        cout << "**************************************************************" << endl;
        cout << "*                            FILA                            *" << endl;
        cout << "*                                                            *" << endl;
        cout << "*      1) Verificar tamanho da Fila e se a Fila esta vazia   *" << endl;
        cout << "*      2) Remover primeiro elemento da Fila vazia            *" << endl;
        cout << "*      3) Retornar ultimo elemento da Fila vazia             *" << endl;
        cout << "*      4) Retornar primeiro elemento da Fila vazia           *" << endl;
        cout << "*                                                            *" << endl;
        cout << "*      0) Voltar                                             *" << endl;
        cout << "*                                                            *" << endl;
        cout << "**************************************************************" << endl << endl;

        cout << "Digite a opcao que deseja:" << endl << "--> ";
        cin >> opcao;
        cout << endl;

        switch (opcao)
        {
            case 1:
                cout << "Tamanho da fila: " << fila.Tamanho () << endl;
                cout << "Verificando se a fila está vazia: ";
                if (fila.Empty () == false)
                    cout << "A fila nao esta vazia" << endl;
                else    
                    cout << "A fila esta vazia" << endl;
                break;
            case 2:
                cout << "Removendo o primeiro elemento da fila ..." << endl;
                fila.Pop ();
                break;
            case 3:
                cout << "Retornando o ultimo elemento da fila: " << fila.Back () << endl;
                break;
            case 4:
                cout << "Retornando o primeiro elemento da fila: " << fila.Front () << endl;
                break;
            case 0:
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }
    }
}   

/** 
 * @brief	Função que realiza testes no tad pilha, implementados na biblioteca
 */
void teste_pilha ()
{
    /** Testando a pilha */
    Pilha <int> pilha;

    int opcao = -1;
    while (opcao != 0)
    {
        cout << endl;
        cout << "**************************************************************" << endl;
        cout << "*                            PILHA                           *" << endl;
        cout << "*                                                            *" << endl;
        cout << "*      1) Verificar tamanho da Pilha e se a Pilha esta vazia *" << endl;
        cout << "*      2) Remover o elemento do topo da Pilha vazia          *" << endl;
        cout << "*      3) Retornar o elemento do topo da Pilha vazia         *" << endl;
        cout << "*                                                            *" << endl;
        cout << "*      0) Voltar                                             *" << endl;
        cout << "*                                                            *" << endl;
        cout << "**************************************************************" << endl << endl;

        cout << "Digite a opcao que deseja:" << endl << "--> ";
        cin >> opcao;
        cout << endl;

        switch (opcao)
        {
            case 1:
                cout << "Tamanho da pilha: " << pilha.Tamanho () << endl;

                cout << "Verificando se a pilha está vazia: ";
                if (pilha.Empty () == false)
                    cout << "A pilha nao esta vazia" << endl;
                else    
                    cout << "A pilha esta vazia" << endl;
                break;
            case 2:
                cout << "Removendo o elemento do topo da pilha ..." << endl;
                pilha.Pop ();
                break;
            case 3:
                cout << "Retornando o elemento do topo da pilha: " << pilha.Top () << endl;
                break;
            case 0:
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }
    }
}

/** 
 * @brief	Função que chama as funcoes que fazem os testes nos TADs
 */
void testes_tads ()
{
    cout << "***********************************************" << endl;
    cout << "*                   AVISO !!!                 *" << endl;
    cout << "*      AO REALIZAR OS TESTES, MUITOS DOS      *" << endl; 
    cout << "*        TRATAMENTOS DE EXCECOES FAZEM        *" << endl;    
    cout << "*      COM QUE O PROGRAMA SEJA ENCERRADO      *" << endl;
    cout << "***********************************************" << endl;

    int opcao = -1;
    while (opcao != 0)
    {
        cout << endl;
        cout << "***********************************************" << endl;
        cout << "*                     TADs                    *" << endl;
        cout << "*                                             *" << endl;
        cout << "*      1) Lista                               *" << endl;
        cout << "*      2) Pilha                               *" << endl;
        cout << "*      3) Fila                                *" << endl;
        cout << "*                                             *" << endl;
        cout << "*      0) Voltar                              *" << endl;
        cout << "*                                             *" << endl;
        cout << "***********************************************" << endl << endl;

        cout << "Digite a opcao que deseja:" << endl << "--> ";
        cin >> opcao;
        cout << endl;

        switch (opcao)
        {
            case 1:
                teste_lista ();
                break;
            case 2:
                teste_pilha ();
                break;
            case 3:
                teste_fila ();
                break;
            case 0:
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }
    }
}