/**
* @file     funcoes_excecao.cpp
* @brief    Implementacao da funcao que verifica se ha alguma excecao no retorno de uma busca
* @author   Pedro Emerick (p.emerick@live.com)
* @since    23/06/2017
* @date	    25/06/2017
*/

#include "funcoes_excecao.h"

#include "excecoes.h"
using namespace edb1;

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

/** 
 * @brief	Função que verifica se ha alguma excecao no retorno da busca 
 *          e faz o tratamento necessario
 * @param 	indice_resultado Retorno da busca
 */
void ErroBusca (int indice_resultado) 
{
    try {
        /** Caso nao encontre o elemento na busca */
        if (indice_resultado == -1) 
            throw ElementoNaoEncontrado ();
        /** Caso o vetor de busca esteja com um tamanho invalido */
        else if (indice_resultado == -2)
            throw VetorVazio ();
        /** Caso nao tenha nenhum erro */
        else
            cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    } catch (ElementoNaoEncontrado &ex) {
        cerr << ex.what () << endl;
    } catch (VetorVazio &ex) {
        cerr << ex.what () << endl;
    } catch (...) {
        cerr << "Erro desconhecido na busca !!!" << endl;
    }
}
