/**
* @file     teste_busca.cpp
* @brief    Implementacao da funcao que faz os testes com os algoritmos de busca
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#include "teste_busca.h"

#include "buscas.h"
#include "quicksort.h"

#include <iostream>
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#include <string>
using std::string;
using std::getline;

#include <sstream> 
using std::ws;

using namespace edb1;

#include "funcoes_vetor.h"

#include "funcoes_excecao.h"

/** 
 * @brief	Função que realiza testes nos algoritmos de busca, implementados na biblioteca
 */
void testes_busca ()
{
    cout << "O vetor usado nestes testes tem tamanho 10 com valores aleatorios";
    cout << endl << endl;

    cout << "*********************************************" << endl;
    cout << "*         TESTANDO ERROS NAS BUSCAS         *" << endl;
    cout << "*********************************************" << endl << endl;

    int vetor [10] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19};
    string vetor_s [10] = {"pedro", "joao" ,"lucas", "valmir", "silvio", "gildo", "raul", "luan", "teixeira", "imd"};
    int indice_resultado;

    cout << "********************************  ELEMENTO BUSCADO NAO ESTA NO VETOR  ********************************" << endl << endl;
    
    cout << "--> BUSCA EM VETOR:" << endl << endl;
    cout << "Vetor para busca: ";
    imprimi_vetor (vetor, 10);

    /** Testando a busca sequencial iterativa */
    cout << endl << "-> BUSCA SEQUENCIAL ITERATIVA: " << endl << endl;
    cout << "Buscando numero 20 no vetor: " << endl;
    indice_resultado = busca_sequencial_iterativa (vetor, 10, 20);
    ErroBusca (indice_resultado);

    /** Testando a busca sequencial recursiva */
    cout << endl << "-> BUSCA SEQUENCIAL RECURSIVA: " << endl << endl;
    cout << "Buscando numero 2 no vetor: " << endl;    
    indice_resultado = busca_sequencial_recursiva (vetor, 10, 2);
    ErroBusca (indice_resultado);
    
    /** Testando a busca binaria recursiva */
    cout << endl << "-> BUSCA BINARIA RECURSIVA: " << endl << endl;
    cout << "Buscando numero 4 no vetor: " << endl;        
    indice_resultado = busca_binaria_recursiva (vetor, 10, 4);
    ErroBusca (indice_resultado);

    /** Testando a busca binaria iterativa */
    cout << endl << "-> BUSCA BINARIA ITERATIVA: " << endl << endl;
    cout << "Buscando numero 8 no vetor: " << endl;        
    indice_resultado = busca_binaria_iterativa (vetor, 10, 8);
    ErroBusca (indice_resultado);

    /** Testando a busca ternaria recursiva */
    cout << endl << "-> BUSCA TERNARIA RECURSIVA: " << endl << endl;
    cout << "Buscando numero 125 no vetor: " << endl;        
    indice_resultado = busca_ternaria_recursiva (vetor, 0, 9, 125);
    ErroBusca (indice_resultado);

    /** Testando a busca ternaria iterativa */
    cout << endl << "-> BUSCA TERNARIA ITERATIVA: " << endl << endl;
    cout << "Buscando numero 25 no vetor: " << endl;        
    indice_resultado = busca_ternaria_iterativa (vetor, 0, 9, 25);
    ErroBusca (indice_resultado);

    cout << endl;
    cout << "********************************  TAMANHO DE VETOR INVALIDO  ********************************" << endl << endl;

    string texto = "teste";

    /** Testando a busca sequencial iterativa */
    cout << "-> BUSCA SEQUENCIAL ITERATIVA: " << endl << endl;
    cout << "Passando tamanho 0: " << endl;            
    indice_resultado = busca_sequencial_iterativa (vetor_s, 0, texto);
    ErroBusca (indice_resultado);
    
    /** Testando a busca sequencial recursiva */
    cout << endl << "-> BUSCA SEQUENCIAL RECURSIVA: " << endl << endl;
    cout << "Passando tamanho -2: " << endl;
    indice_resultado = busca_sequencial_recursiva (vetor_s, -2, texto);
    ErroBusca (indice_resultado);
    
    /** Testando a busca binaria recursiva */
    cout << endl << "-> BUSCA BINARIA RECURSIVA: " << endl << endl;
    cout << "Passando tamanho -1: " << endl;
    indice_resultado = busca_binaria_recursiva (vetor_s, -1, texto);
    ErroBusca (indice_resultado);

    /** Testando a busca binaria iterativa */
    cout << endl << "-> BUSCA BINARIA ITERATIVA: " << endl << endl;
    cout << "Passando tamanho -4: " << endl;
    indice_resultado = busca_binaria_iterativa (vetor_s, -4, texto);
    ErroBusca (indice_resultado);

    /** Testando a busca ternaria recursiva */
    cout << endl << "-> BUSCA TERNARIA RECURSIVA: " << endl << endl;
    cout << "Passando indice do fim do vetor -24: " << endl;
    indice_resultado = busca_ternaria_recursiva (vetor_s, 0, -24, texto);
    ErroBusca (indice_resultado);

    /** Testando a busca ternaria iterativa */
    cout << endl << "-> BUSCA TERNARIA ITERATIVA: " << endl << endl;
    cout << "Passando indice do inicio do vetor -13: " << endl;
    indice_resultado = busca_ternaria_iterativa (vetor_s, -13, 9, texto);
    ErroBusca (indice_resultado);
}